const pessoas = require('../db/banco')

class PessoaControler {

    listar(requisicao, resposta) {
        resposta.json(pessoas);
    }

    buscar(req, res) {
        // let pessoaLoop;
        // for(var idx = 0; idx < pessoas.length; idx++){
        //     const p = pessoas[idx];
        //     if (p.codigo == codigo){
        //         pessoaLoop = p;
        //         break;
        //     }
        // }

        const codigo = req.params.codigo;
        const pessoa = pessoas.find(p => p.codigo == codigo);
        res.json(pessoa);
    }

    salvar(req, res){
        const pessoa = req.body;
        pessoas.push(pessoa);
        res.json(pessoas);
    }

    atualizar(req, res){
        const codigo = req.params.codigo;
        const pessoa = req.body;

        const index = pessoas.findIndex(p => p.codigo == codigo);
        pessoas[index] = pessoa;
        res.json(pessoas);
    }

    excluir(req, res){
        const codigo = req.params.codigo;
        const index = pessoas.findIndex(p => p.codigo == codigo);
        pessoas.splice(index, 1);
        res.json(pessoas);
    }
}

module.exports = new PessoaControler();
