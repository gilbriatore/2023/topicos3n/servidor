const express = require('express');
const servidor = express();
servidor.use(express.json());

const pessoaController = require('./controllers/pessoaController');

servidor.get('/pessoas', pessoaController.listar);
servidor.get('/pessoas/:codigo', pessoaController.buscar);
servidor.post('/pessoas', pessoaController.salvar);
servidor.put('/pessoas/:codigo', pessoaController.atualizar);
servidor.delete('/pessoas/:codigo', pessoaController.excluir);

servidor.get('/', function(req, res){    
    res.send('Servidor de APIs rodando...');
});

servidor.listen(3020, function(){
    console.log('Servidor rodando na porta 3020...');
});
