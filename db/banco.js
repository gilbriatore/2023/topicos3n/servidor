const pessoas = [
    {
        codigo: 1,
        nome: 'João',
        idade: 23
    },
    {
        codigo: 2,
        nome: 'Ana',
        idade: 19
    },
    {
        codigo: 3,
        nome: 'Pedro',
        idade: 20
    }
];

module.exports = pessoas;